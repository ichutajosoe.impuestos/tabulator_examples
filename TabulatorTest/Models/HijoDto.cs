using System;

namespace TabulatorTest.Models
{
    public class HijoDto
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public long NumOrden { get; set; }
        public long NumOrdenPaga { get; set; }
        public DateTime Fecha { get; set; }
    }
}