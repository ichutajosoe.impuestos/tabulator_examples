using System;
using System.Collections.Generic;

namespace TabulatorTest.Models
{
    public class BoletaDto
    {
        public int Id { get; set; }
        public string Descripcion { get; set; }
        public long NumOrden { get; set; }
        public long NumOrdenPaga { get; set; }
        public DateTime Fecha { get; set; }
        public List<HijoDto> _children { get; set; }
    }
}