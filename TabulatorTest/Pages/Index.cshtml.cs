﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using TabulatorTest.Models;

namespace TabulatorTest.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public List<BoletaDto> boletas = new List<BoletaDto>();
        public List<HijoDto> hijos = new List<HijoDto>();
        public string dataJson;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
        }

        public void OnGet()
        {
            for (int i = 0; i < 2; i++)
            {
                
                HijoDto hijo = new HijoDto()
                {
                    Id = i,
                    NumOrden = 2 * i,
                    NumOrdenPaga = 2 + i,
                    Descripcion = "Hijos",
                    Fecha = DateTime.Now,
                };
                hijos.Add(hijo);
            }
            
            for (int i = 0; i < 20; i++)
            {
                
                BoletaDto Descargo = new BoletaDto()
                {
                    Id = i,
                    NumOrden = 2 * i,
                    NumOrdenPaga = 0,
                    Descripcion = "Padre",
                    Fecha = DateTime.Now,
                    _children = (((i%2)==0)? hijos: null),
                    // _children = hijos,
                };
                boletas.Add(Descargo);
            }
            dataJson = JsonConvert.SerializeObject(boletas);
        }
    }
}